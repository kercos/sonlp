from gensim.models import Word2Vec
from params import glove_model, word2vec_model

# you need to download the models from http://hlt.isti.cnr.it/wordembeddings/

# model = Word2Vec.load(glove_model)
model = Word2Vec.load(word2vec_model)

def test():        

	# for word in model.vocab[:10]: # here you get the words
	#     print(word,model.vocab[word]) # frequency stats

	model.wv['sole'] # here you get the numpy embedding vectors
	# Out: 
	# array([ -1.86661184e-02,   1.31065890e-01,   3.69563736e-02,
	#         -6.03673719e-02,   6.20404482e-02,   5.64207993e-02,.......

	model.wv.most_similar('casa')
	model.most_similar(positive=['donna', 're'], negative=['uomo'])

	words = ['casa', 'edificio', 'banana', 'mela']
	for i in range(len(words)):
		for j in range(i+1,len(words)):
				# print('{}\t{} - {}'.format(words[i].similarity(words[j]), words[i], words[j]))
				print('{}\t{} - {}'.format(model.wv.similarity(words[i],words[j]), words[i], words[j]))
        
def plot_words(word_list):
	from sklearn.manifold import TSNE	
	import pandas as pd
	import matplotlib.pyplot as plt
	X = model[word_list]
	tsne = TSNE(n_components=2)
	X_tsne = tsne.fit_transform(X)
	df = pd.DataFrame(X_tsne, index=word_list, columns=['x', 'y'])
	fig = plt.figure()
	ax = fig.add_subplot(1, 1, 1)
	ax.scatter(df['x'], df['y'])
	for word, pos in df.iterrows():
		ax.annotate(word, pos)
	plt.show()

def intruso(word_list):
	for i in range(len(word_list)):
		for j in range(i+1,len(word_list)):
			print('{}\t{} - {}'.format(model.wv.similarity(word_list[i],word_list[j]), word_list[i], word_list[j]))

if __name__ == "__main__":
	#test()
	word_list = ['ambiguo','oscuro','univoco','enigmatico','equivoco']
	intruso(word_list)
	# plot_words(word_list)
        
