import nltk
import params
nltk.data.path.append(params.nltk_data_dir)


# lista = ['uno','due','tre']
# ' '.join(lista)
# '-'.join(lista)

# f_in = open(pinocchio_file)
# lista_righe = f_in.readlines()
# pinocchio_raw = ''.join(lista_righe)
# f_in.close()

# alternatively use the with statement:
# With the "With" statement, you get better syntax and exceptions handling. 
# The with statement simplifies error handling by encapsulating common
# preparation and cleanup tasks."
# In addition, it will automatically close the file. The with statement provides
# a way for ensuring that a clean-up is always used.

pinocchio_file = '../CORPORA/pinocchio_clean.txt'
with open(pinocchio_file) as f_in:
    lista_righe = ''.join(f_in.readlines())

pinocchio_raw = ''.join(lista_righe)

# word tokenizer
from nltk import word_tokenize

pinocchio_tokens = word_tokenize(pinocchio_raw.lower())
pinoccho_tokens_set = set(pinocchio_tokens)

pinocchio = nltk.Text(pinocchio_tokens)
pinocchio.concordance("cattivo")

pinocchio.similar("mangiare")

pinocchio.common_contexts(["casa", "scuola"])
pinocchio.common_contexts(["pinocchio"])

len(pinocchio)
sorted(set(pinocchio))
len(set(pinocchio))
len(set(pinocchio)) / len(pinocchio) # lexical richness
pinocchio.count("pinocchio")

# lexical distribution
fdist = nltk.FreqDist(pinocchio)
print(fdist)
fdist.most_common(50) 
fdist.plot(50, cumulative=True)
fdist.plot(50, cumulative=False)

# remove stop-words
from nltk.corpus import stopwords
stop_words_it = set(stopwords.words('italian'))
punctuation = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~—'
pinocchio_no_stopwords = [w for w in pinocchio if w.lower() not in stop_words_it and not any(p in w for p in punctuation)]

fdist_no_stopwords = nltk.FreqDist(pinocchio_no_stopwords)
fdist_no_stopwords.most_common(50) 
fdist_no_stopwords.plot(50, cumulative=False)

# dispersion plot
pinocchio.dispersion_plot(["pinocchio", "geppetto", "fata", "gatto", "volpe"])

# Collocations
bigrams = [' '.join(bi) for bi in nltk.bigrams(pinocchio) if not any(s in bi for s in stop_words_it) and not any(p in bi for p in punctuation)]
fdist = nltk.FreqDist(bigrams)
fdist.most_common(50) 

pinocchio.collocations()
