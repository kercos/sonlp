import nltk
import params
nltk.data.path.append(params.nltk_data_dir)

from nltk import word_tokenize
from nltk.lm import MLE
from nltk.lm.preprocessing import pad_both_ends, padded_everygram_pipeline

s = "Natural-language processing (NLP) is an area of computer science "     "and artificial intelligence concerned with the interactions "     "between computers and human (natural) languages."
s = s.lower()

order = 3

paddedLine = [list(pad_both_ends(word_tokenize(s), n=order))]

train, vocab = padded_everygram_pipeline(order, paddedLine)
lm = MLE(order)
lm.fit(train, vocab)

lm.generate(4, text_seed=['is','an'])

lm.generate(4, text_seed=['and'])

