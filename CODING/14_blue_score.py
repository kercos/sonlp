from nltk.translate.bleu_score import SmoothingFunction
chencherry = SmoothingFunction()

# https://machinelearningmastery.com/calculate-bleu-score-for-text-python/ 

from nltk.translate.bleu_score import sentence_bleu
reference = [['funerary', 'inscription', 'b']]
candidate = ['funerary', 'inscription', 'c']
print('Cumulative 1-gram: %f' % sentence_bleu(reference, candidate, weights=(1, 0, 0, 0), smoothing_function=chencherry.method1))
print('Cumulative 2-gram: %f' % sentence_bleu(reference, candidate, weights=(0.5, 0.5, 0, 0), smoothing_function=chencherry.method1))
print('Cumulative 3-gram: %f' % sentence_bleu(reference, candidate, weights=(0.33, 0.33, 0.33, 0), smoothing_function=chencherry.method1))
print('Cumulative 4-gram: %f' % sentence_bleu(reference, candidate, weights=(0.25, 0.25, 0.25, 0.25), smoothing_function=chencherry.method1))

# from nltk.translate.bleu_score import sentence_bleu
# reference = [['funerary', 'inscription']]
# candidate = ['funerary', 'inscription']
# score = sentence_bleu (reference, candidate, smoothing_function=chencherry.method1)
# print(score)

# from nltk.translate.bleu_score import sentence_bleu
# reference = [['miniature', 'container']]
# candidate = ['miniaturistic', 'container']
# score = sentence_bleu (reference, candidate, smoothing_function=chencherry.method1)
# print(score)

# from nltk.translate.bleu_score import sentence_bleu
# reference = ['the cat is on the mat'.split(), 'there is a cat on the mat'.split()]
# candidate = 'the the the the the the the'.split()
# score = sentence_bleu (reference, candidate, smoothing_function=chencherry.method1)
# print(score)


# from nltk.translate.bleu_score import sentence_bleu
# reference = [['this', 'is', 'a', 'test'], ['this', 'is' 'test']]
# candidate = ['this', 'is', 'a', 'test']
# score = sentence_bleu(reference, candidate)
# print(score)


# from nltk.translate.bleu_score import sentence_bleu
# reference = [['this', 'is', 'a', 'test'], ['this', 'is' 'test']]
# candidate = ['this', 'is', 'a', 'new', 'test']
# score = sentence_bleu(reference, candidate)
# print(score)

