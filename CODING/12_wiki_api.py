import wikipediaapi

wiki_wiki = wikipediaapi.Wikipedia('it')

def traduci_termine(termine_ita, codice_lang):    
    page_py = wiki_wiki.page(termine_ita)
    # page_py.exists()
    # page_py.title
    # page_py.summary
    # page_py.fullurl
    # page_py.text
    langlinks = page_py.langlinks
    # type(langlinks)    
    if codice_lang in langlinks:
        page_py_lang = langlinks[codice_lang]
        traduzione = page_py_lang.title
        print("{} -> {}".format(termine_ita, traduzione))
    else:
        print("{} -> -".format(termine_ita))
    # return traduzione

lista = ['anfora','semicolonna','onichoe','caccabus']
for lemma in lista:
    # page_py = wiki_wiki.page(lemma)
    # print("{} -> {}".format(lemma, page_py.summary))
    traduci_termine(lemma, 'en')
    traduci_termine(lemma, 'fr')