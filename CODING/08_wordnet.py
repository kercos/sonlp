import nltk
import params
nltk.data.path.append(params.nltk_data_dir)

from nltk.corpus import wordnet as wn

"""
An NLTK interface for WordNet

WordNet is a lexical database of English.
Using synsets, helps find conceptual relationships between words
such as hypernyms, hyponyms, synonyms, antonyms etc.

For details about WordNet see:
http://wordnet.princeton.edu/

This module also allows you to find lemmas in languages
other than English from the Open Multilingual Wordnet
http://compling.hss.ntu.edu.sg/omw/
"""

# We can check what is the synset of the word motorcar:
wn.synsets('motorcar')
# [Synset('car.n.01')]
# motorcar has just one possible meaning (synset). 
# It is identified by car.n.01 (we will call it "lemma code name") - 
# the first noun (letter n) sense of car.

# You can dig in and see what are other words within this particular synset:
wn.synset('car.n.01').lemma_names()
# ['car', 'auto', 'automobile', 'machine', 'motorcar']
# All of the 5 words have the same meaning — a car (more precisely — car.n.01).

car_lemmas = wn.synset('car.n.01').lemmas()
for l in car_lemmas:
    print(l.name())

# But as you might expect a word might be ambiguous, for example, a printer:
wn.synsets('printer')
# [Synset('printer.n.01'), Synset('printer.n.02'), Synset('printer.n.03')]

# You see that there are 3 possible contexts. 
# To help understand the meaning of each one we can see it’s definition 
# and provided examples (if are available).

for synset in wn.synsets('printer'):
    print("\tLemma: {}".format(synset.name()))
    print("\tDefinition: {}".format(synset.definition()))
    print("\tExample: {}".format(synset.examples()))

# let's check what words (lemmas) are included in each lemma
wn.synset('printer.n.01').lemmas()
# [Lemma('printer.n.01.printer'), Lemma('printer.n.01.pressman')] 
wn.synset('printer.n.02').lemmas()
# [Lemma('printer.n.02.printer')]
wn.synset('printer.n.03').lemmas()
# [Lemma('printer.n.03.printer'), Lemma('printer.n.03.printing_machine')]

wn.synsets('help') #  NOUN, ADJ, VERB and ADV.
wn.synsets('help', pos=wn.VERB) #  NOUN, ADJ, VERB and ADV.

# hyponyms -> a word of more specific meaning
dog = wn.synset('dog.n.01')
dog.definition()
cat = wn.synset('cat.n.01')
cat.definition()
dog_hypo = dog.hyponyms()
sorted([lemma.name() for synset in dog_hypo for lemma in synset.lemmas()])
cat_hypo = cat.hyponyms()
domestic_cat = wn.synset('domestic_cat.n.01')
domestic_cat_hypo = domestic_cat.hyponyms()
sorted([lemma.name() for synset in cat_hypo for lemma in synset.lemmas()])
sorted([lemma.name() for synset in domestic_cat_hypo for lemma in synset.lemmas()])

# hypernyms -> a word with a broad meaning
dog_hyper = dog.hypernyms()
sorted([lemma.name() for synset in dog_hyper for lemma in synset.lemmas()])
cat_hyper = cat.hypernyms()
sorted([lemma.name() for synset in cat_hyper for lemma in synset.lemmas()])

dog.lowest_common_hypernyms(cat)

# hypernyms -> a word with a broad meaning
good_lemma = wn.synset('good.a.01').lemmas()[0]
sorted([lemma.name() for lemma in good_lemma.antonyms()])

# meronyms -> part of something
tree = wn.synset('tree.n.01')
# part_meronyms() - obtains parts,
tree.part_meronyms()
# [Synset('burl.n.02'), Synset('crown.n.07'), Synset('limb.n.02'), Synset('stump.n.01'), Synset('trunk.n.01')]
# substance_meronyms() - obtains substances
tree.substance_meronyms()
# [Synset('heartwood.n.01'), Synset('sapwood.n.01')]

# holonyms - membership to something
wn.synset('atom.n.01').part_holonyms()
# [Synset('chemical_element.n.01'), Synset('molecule.n.01')]
wn.synset('hydrogen.n.01').substance_holonyms()
# [Synset('water.n.01')]

# Entailment -> how verbs are related by 'cause' relation
wn.synset('eat.v.01').entailments()
# [Synset('chew.v.01'), Synset('swallow.v.01')]

#############
# VERB FRAMES
#############

# https://wordnet.princeton.edu/documentation/wninput5wn
wn.synset('think.v.01').frame_ids()

for lemma in wn.synset('think.v.01').lemmas():
    print(lemma, lemma.frame_ids())
    print(" | ".join(lemma.frame_strings()))

for lemma in wn.synset('stretch.v.02').lemmas():
    print(lemma, lemma.frame_ids())
    print(" | ".join(lemma.frame_strings()))

#############
# SYMILARITY
#############

'''
Return a score denoting how similar two word senses are, 
based on the shortest path that connects the senses in the is-a 
(hypernym/hypnoym) taxonomy. 
The score is in the range 0 to 1. 
By default, there is now a fake root node added to verbs so for cases where previously 
a path could not be found---and None was returned---it should return a value. 
The old behavior can be achieved by setting simulate_root to be False. 
A score of 1 represents identity i.e. comparing a sense with itself will return 1.
'''

train = wn.synset('train.n.01')
horse = wn.synset('horse.n.01')
animal = wn.synset('animal.n.01')
atom = wn.synset('atom.n.01')
 
print("Horse => Horse: {}".format(horse.path_similarity(horse)))
# Horse => Horse: 1.0
print("Train => Horse: {}".format(train.path_similarity(horse)))
#Train => Horse: 0.058823529411764705
print("Horse => Train: {}".format(horse.path_similarity(train)))
# Horse => Train: 0.058823529411764705
print("Horse => Animal: {}".format(horse.path_similarity(animal)))
# Horse => Animal: 0.1111111111111111
print("Train => Atom: {}".format(train.path_similarity(atom)))
# Train => Atom: 0.09090909090909091
print("Animal => Atom: {}".format(animal.path_similarity(atom)))
# Animal => Atom: 0.1111111111111111

#############
# All Synsets
#############

# ADJ, ADV, NOUN, VERB = 'a', 'r', 'n', 'v'

wn.all_synsets('r') # adverbs
wn.all_synsets('n')
wn.all_synsets('a') # adj
wn.all_synsets('v')


###############
# Depth
###############

wn.synset('entity.n.01').min_depth()
wn.synset('car.n.01').min_depth()
wn.synset('horse.n.01').min_depth()
wn.synset('mare.n.01').min_depth()
wn.synset('animal.n.01').min_depth()


###############
# Multilingual
###############
sorted(wn.langs())
spy = wn.synset('spy.n.01')
spy.lemma_names(lang='ita')
cane_synset = wn.synsets('cane', lang='ita')
sinonimi_cane_lemmas = [s.lemmas(lang='ita') for s in cane_synset]
cane_lemmas = wn.lemmas('cane', lang='ita')

###############
# MWE
###############
synsets = list(wn.all_synsets())
# len(synsets)
# 117659
mw_synstes = [s for s in synsets if any('_' in l.name() for l in s.lemmas()) ]
# len(mw_synstes)
# 46470
mw_two_lemmas = [l.name().split('_') for s in mw_synstes for l in s.lemmas() if '_' in l.name() and l.name().count('_')==1]
# len(mw_two_lemmas)
# 58227
