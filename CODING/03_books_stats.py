import nltk
import params
nltk.data.path.append(params.nltk_data_dir)

from nltk.book import *

'''
text1: Moby Dick by Herman Melville 1851
text2: Sense and Sensibility by Jane Austen 1811
text3: The Book of Genesis
text4: Inaugural Address Corpus
text5: Chat Corpus
text6: Monty Python and the Holy Grail
text7: Wall Street Journal
text8: Personals Corpus
text9: The Man Who Was Thursday by G . K . Chesterton 1908
'''

# A concordance view shows us every occurrence of a given word, together with some context. 
text1.concordance("monstrous")

# What other words appear in a similar range of contexts?
# We can find out by appending the term similar to the name of the text in question, 
# then inserting the relevant word in parentheses:
text1.similar("monstrous")

# The term common_contexts allows us to examine just the contexts 
# that are shared by two or more words, such as monstrous and very. 
# We have to enclose these words by square brackets as well as parentheses, 
# and separate them with a comma:
text2.common_contexts(["monstrous", "very"])

len(text3)
sorted(set(text3))
len(set(text3))

# lexical richness
len(set(text3)) / len(text3) 

text3.count("help")

# lexical distribution
fdist = nltk.FreqDist(text1)
print(fdist)
fdist.most_common(50) 
fdist.plot(50, cumulative=True)
fdist.plot(50, cumulative=False)

# remove stop-words
from nltk.corpus import stopwords
punctuation = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~—'
stop_words_en = set(stopwords.words('english'))
# remove punctuation and stop-words
text1_no_stopwords = [w for w in text1 if w.lower() not in stop_words_en and not any(p in w for p in punctuation)]
fdist_no_stopwords = nltk.FreqDist(text1_no_stopwords)
fdist_no_stopwords.most_common(50) 
fdist_no_stopwords.plot(50, cumulative=False)

# we can also determine the location of a word in the text: 
# how many words from the beginning it appears. 
# This positional information can be displayed using a dispersion plot.
text4.dispersion_plot(["citizens", "democracy", "freedom", "duties", "America"])

# Collocations
bigrams = [' '.join(bi) for bi in nltk.bigrams(text4) if not any(s in bi for s in stop_words_en) and not any(p in bi for p in punctuation)]
fdist = nltk.FreqDist(bigrams)
fdist.most_common(50) 

text4.collocations()
