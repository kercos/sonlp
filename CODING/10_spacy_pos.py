import spacy
#from spacy import displacy

nlp = spacy.load('it_core_news_sm')

def pos_tag(sentence):
    print("Analisi di: {}".format(sentence))
    doc = nlp(sentence)
    for token in doc:
        print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_, token.shape_, token.is_alpha, token.is_stop)
    print('')

'''
universaldependencies tagset
https://universaldependencies.org/u/pos/
ADJ: adjective
ADP: adposition
ADV: adverb
AUX: auxiliary
CCONJ: coordinating conjunction
DET: determiner
INTJ: interjection
NOUN: noun
NUM: numeral
PART: particle
PRON: pronoun
PROPN: proper noun
PUNCT: punctuation
SCONJ: subordinating conjunction
SYM: symbol
VERB: verb
X: other
'''

# main function
if __name__ == "__main__":
    lista_frasi = [
        "Questa è una frase di prova.",
        "La mattina ha l'oro in bocca.",
        "L'albero del mio giardino è più verde del tuo."
    ]
    for s in lista_frasi:
        pos_tag(s)
