nltk_data_dir = "/Users/fedja/scratch/CORPORA/nltk_data"
# nltk_data_dir = "../NLTK_data"
# nltk_data_dir = "../Word2Vec"

# you need to download the models from http://hlt.isti.cnr.it/wordembeddings/
glove_model = '/Users/fedja/scratch/CORPORA/Word2Vec/hlt.isti.cnr/glove/glove_WIKI'
word2vec_model = '/Users/fedja/scratch/CORPORA/Word2Vec/hlt.isti.cnr/word2vec/models/wiki_iter=5_algorithm=skipgram_window=10_size=300_neg-samples=10.m'

# glove_model = '../Word2Vec/hlt.isti.cnr/glove/glove_WIKI'
# word2vec_model = '../Word2Vec/hlt.isti.cnr/word2vec/models/wiki_iter=5_algorithm=skipgram_window=10_size=300_neg-samples=10.m'
