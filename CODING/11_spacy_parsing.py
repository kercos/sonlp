import spacy
from spacy import displacy

nlp = spacy.load('it_core_news_sm')

# syntactic relations 
# https://universaldependencies.org/u/dep/index.html

def dependecy(sentence):    
    doc = nlp(sentence)
    displacy.serve(doc, style='dep')
    # goto http://localhost:5000/

if __name__ == "__main__":
    #sentence = "Il ragazzo cammina per strada"
    sentence = "Mi ha detto che sta"
    dependecy(sentence)