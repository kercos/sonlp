
import nltk
import params
nltk.data.path.append(params.nltk_data_dir)

from nltk import word_tokenize
from nltk.lm import MLE
from nltk.lm.preprocessing import pad_both_ends, padded_everygram_pipeline

def generate_sentence(order, num_words, initial_word):

    sent_detector = nltk.data.load('tokenizers/punkt/italian.pickle')

    pinocchio_file = '../CORPORA/pinocchio_clean.txt'

    with open(pinocchio_file) as f_in:
        pinocchio_raw = ''.join(f_in.readlines())

    pinocchio_sentences = sent_detector.tokenize(pinocchio_raw)

    padded_lines = []
    for sentence in pinocchio_sentences:
        sentence_tokens = word_tokenize(sentence.lower())
        padded_lines.append(list(pad_both_ends(sentence_tokens, n=order)))

    train, vocab = padded_everygram_pipeline(order, padded_lines)

    lm = MLE(order)
    lm.fit(train, vocab)

    #lm.generate(4, text_seed=['pinocchio'])

    #' '.join(lm.generate(50, text_seed=['pinocchio']))

    sentence = ' '.join(lm.generate(num_words, text_seed = [ initial_word ] ) )
    
    print(sentence)

generate_sentence(order=5, num_words=50, initial_word='chiamatemi')