1 + 5 * 2 - 3

#1 +
# returns error

# variables
a = 'questa è una stringa'
a
a = "questa è una stringa"
a
b = a
b

# print
print(a)
print(b)
print(a,b)
print('Il valore di "a" è "{}"'.format(a))

# string operations

# concatenation
a = "uno"
b = "due"
c = a + b
print(c)
print("{}{}".format(a,b))
print("{} {}".format(a,b))

s = 'viaggio' 
s_lower = s.lower()
s_lower == s
s.upper()
s.title()

s.startswith('o')	#test if s starts with t
s.endswith('t')	#test if s ends with t
'g' in s	#test if t is a substring of s
'G'.lower() in s.lower()

s.islower()	#test if s contains cased characters and all are lowercase
s.isupper()	#test if s contains cased characters and all are uppercase

s.isalpha()	#test if s is non-empty and all characters in s are alphabetic
s.isalnum()	#test if s is non-empty and all characters in s are alphanumeric
s.isdigit()	#test if s is non-empty and all characters in s are digits
s.istitle()	#test if s contains cased characters and is titlecased (i.e. all words in s have initial capitals)

s.isalpha() and s.islower()
s.isalpha() or s.islower()

#ditance
import nltk
from nltk.metrics.distance import edit_distance
edit_distance('casa','caso')
edit_distance('casa','house')

# indexing
a = 'questa è una stringa'
#    01234567890123456789
len(a) # lunghezza di una stringa
a[0] # carattere all'indice 0 (iniziale)
a[1] # carattere all'indice 1
a[-1] # carattere all'ultima posizione
a[3:5] # carattere dall'indice 3 (incluso) all'indice 5 (escluso)

# loops
for c in a:
    print(c)
    for c in a:
        print('-->{}'.format(c))


# list
lista = ['uno','due','tre','quattro','tre']
lista
len(lista)
sorted(lista)

lista = ['Uno','DUE','tre','124']

# iterare con un ciclo for sulla lista
# stampare solo gli elementi 
# a. che contengono la lettera 'U'
# b. che siano lowercase
# b. che siano numerici

for elemento in lista:
    if 'U' in elemento:
        # then
        print("U è presente in {}".format(elemento))    
    else:
        print("U non è presente in {}".format(elemento))    
        
# set
insieme = {'uno', 'due', 'tre', 'tre', 'quattro'}
insieme
insieme = set(['uno','due','tre','quattro','tre'])
insieme
len(insieme)


# dictionaries

diz = {
    'casa': 'house',
    'giovane': 'young',
    'albero': 'tree' 
}

diz['casa']
diz['strada']
diz['strada'] = 'street'
diz.keys()
diz.values()

diz = {
    'casa': {
        'en':'house',
        'fr':'maison'
    },
    'giovane': {
        'en':'young',
        'fr':'jeune'
    },
    'albero': {
        'en':'tree',
        'fr':'arbre',
    }
}

type(diz)
type(lista)
type('stringa di testo')
type(123)