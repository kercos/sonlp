from nltk.parse.generate import generate #, demo_grammar
from nltk import CFG

demo_grammar = \
'''
S -> NP VP
NP -> Det N
PP -> P NP
VP -> V0 | VT NP | V2 NP NP| V2 NP | VI PP
Det -> 'the' | 'a'
N -> 'man' | 'park' | 'dog'
P -> 'in' | 'with'
V0 -> 'slept'
V2 -> 'give'
VT -> 'saw' 
VI -> 'walked'
'''

grammar = CFG.fromstring(demo_grammar)
print(grammar)

for sentence in generate(grammar, n=10):
    print(' '.join(sentence))

for sentence in generate(grammar, depth=4):
    print(' '.join(sentence))

demo_grammar_it = \
'''
S -> S_tipo1 | S_tipo2
S -> NP VP
NP -> art noun
VP -> verb NP
art -> 'il' | 'un'
noun -> 'cane' | 'bambino' | 'panino'
verb -> 'mangia' | 'morde' | 'saluta'
'''

grammar = CFG.fromstring(demo_grammar_it)

for sentence in generate(grammar, n=10):
    print(' '.join(sentence))

for sentence in generate(grammar, depth=4):
    print(' '.join(sentence))
